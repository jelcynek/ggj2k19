﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WhatHomeMeansToYou
{
    // Singleton pattern http://csharpindepth.com/Articles/General/Singleton.aspx
    // Singleton probably should be replaced with DI, here is article about DI in monogame
    // http://becdetat.com/adding-dependency-injection-to-a-monogame-application.html
    //
    public sealed class Map
    {
        private static readonly Lazy<Map> lazy =
            new Lazy<Map>(() => new Map());

        public static Map Instance { get { return lazy.Value; } }

        // lame collisons
        public List<Vector2> floorArea = new List<Vector2>();
        public List<List<Vector2>> listOfColliders = new List<List<Vector2>>();



        private Map()
        {
            // TODO: LAME WAY TO DO COLLISIONS, NEEDS TO BE CHANGED TO TILE COLLISIONS
            var floorCollider = new List<Vector2>();
            floorArea.Add(new Vector2(114, 62));
            floorArea.Add(new Vector2(198, 104));
            floorArea.Add(new Vector2(133, 135));
            floorArea.Add(new Vector2(48, 94));

            var piekarnikCollider = new List<Vector2>();
            piekarnikCollider.Add(new Vector2(152, 82));
            piekarnikCollider.Add(new Vector2(152, 103));
            piekarnikCollider.Add(new Vector2(168, 107));
            piekarnikCollider.Add(new Vector2(180, 100));
            piekarnikCollider.Add(new Vector2(170, 85));
            listOfColliders.Add(piekarnikCollider);
        }

        public void DrawColliders(SpriteBatch spriteBatch, Texture2D pixelTexture)
        {
            Util.DrawPolygon(floorArea, spriteBatch, pixelTexture);
            foreach (var collider in listOfColliders)
            {
                Util.DrawPolygon(collider, spriteBatch, pixelTexture);
            }
        }
    }
}
