﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Animations;
using MonoGame.Extended.Sprites;
using WhatHomeMeansToYou.Interfaces;

namespace WhatHomeMeansToYou
{
    class Item : IGameEntity
    {
        public AnimatedSprite sprite;
        public AnimatedSprite mainSprite;
        public AnimatedSprite secondSprite;
        
        public Item(AnimatedSprite mainSprite, AnimatedSprite secondSprite = null)
        {
            this.mainSprite = this.sprite = mainSprite;
            this.secondSprite = secondSprite;
        }

        public void Update(GameTime gameTime)
        {
            if (sprite != null) sprite.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (sprite != null) spriteBatch.Draw(sprite);
        }

        public bool Action()
        {
            if (this.secondSprite != null)
            {
                if (this.sprite == this.mainSprite) this.sprite = this.secondSprite;
                else this.sprite = this.mainSprite;
            }
            return true;
        }
    }
}
