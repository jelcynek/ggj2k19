﻿using Microsoft.Xna.Framework.Input;

namespace WhatHomeMeansToYou
{
    static class KeySchemaTemplatesFactory
    {
        public static KeySchema getDefaultKeyboardPlayerOneSchema()
        {
            var keySchema = new KeySchema();
            keySchema.Up = Keys.Up;
            keySchema.Down = Keys.Down;
            keySchema.Left = Keys.Left;
            keySchema.Right = Keys.Right;
            keySchema.Action = Keys.Space;
            return keySchema;
        }

        public static KeySchema getDefaultKeyboardPlayerTwoSchema()
        {
            var keySchema = new KeySchema();
            keySchema.Up = Keys.W;
            keySchema.Down = Keys.S;
            keySchema.Left = Keys.A;
            keySchema.Right = Keys.D;
            keySchema.Action = Keys.LeftControl;
            return keySchema;
        }
    }

    struct KeySchema
    {
        public Keys Up;
        public Keys Down;
        public Keys Left;
        public Keys Right;
        public Keys Action;
    }
}
