﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using MonoGame.Extended.TextureAtlases;
using MonoGame.Extended.Animations;
using MonoGame.Extended.Animations.SpriteSheets;
using MonoGame.Extended.Sprites;
using WhatHomeMeansToYou.Interfaces;

namespace WhatHomeMeansToYou
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch targetBatch;
        RenderTarget2D target;

        Texture2D backgroundTexture;
        Texture2D pixelTexture;
        Sprite kontaktSprite;
        List<IGameEntity> gameEntities = new List<IGameEntity>();
        Dictionary<string, Item> items = new Dictionary<string, Item>();

        KeyboardState previousKeyboardState;
        KeyboardState keyboardState;

        bool debugFlag = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = Config.ScaledGameScreenWidth;
            graphics.PreferredBackBufferHeight = Config.ScaledGameScreenHeight;
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            previousKeyboardState = keyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            targetBatch = new SpriteBatch(GraphicsDevice);
            target = new RenderTarget2D(GraphicsDevice, Config.GameScreenWidth, Config.GameScreenHeight);

            backgroundTexture = this.Content.Load<Texture2D>("images/background");
            pixelTexture = new Texture2D(GraphicsDevice, 1, 1);
            pixelTexture.SetData<Color>(new Color[1] { Color.Black });

            var playerOneTexture = this.Content.Load<Texture2D>("spritesheets/player1_spritesheet");
            var playerOneMap = Content.Load<Dictionary<string, Rectangle>>("spritesheets/player1dict");
            var playerOneAtlas = new TextureAtlas("Player", playerOneTexture, playerOneMap);
            var playerOneAnimationFactory = new SpriteSheetAnimationFactory(playerOneAtlas);
            playerOneAnimationFactory.Add("Up", new SpriteSheetAnimationData(new[] { 0, 2, 0, 3 }));

            var ssss = new AnimatedSprite(playerOneAnimationFactory, "Up");

            playerOneAnimationFactory.Add("Down", new SpriteSheetAnimationData(new[] { 4, 6, 4, 7 }));
            playerOneAnimationFactory.Add("Left", new SpriteSheetAnimationData(new[] { 8, 10, 8, 11 }));
            playerOneAnimationFactory.Add("Right", new SpriteSheetAnimationData(new[] { 12, 14, 12, 15 }));
            playerOneAnimationFactory.Add("Idle", new SpriteSheetAnimationData(new[] { 4 }));

            gameEntities.Add(new Player(new Vector2(Config.GameScreenWidth / 2.0f, Config.GameScreenHeight / 2.0f),
                                        PreparePlayerSprites(playerOneAnimationFactory),
                                        KeySchemaTemplatesFactory.getDefaultKeyboardPlayerOneSchema(),
                                        items));
                
            var playerTwoTexture = this.Content.Load<Texture2D>("spritesheets/player2_spritesheet");
            var playerTwoMap = Content.Load<Dictionary<string, Rectangle>>("spritesheets/player2dict");
            var playerTwoAtlas = new TextureAtlas("Player2", playerTwoTexture, playerTwoMap);
            var playerTwoAnimationFactory = new SpriteSheetAnimationFactory(playerTwoAtlas);
            playerTwoAnimationFactory.Add("Up", new SpriteSheetAnimationData(new[] { 0, 2, 0, 3 }));
            playerTwoAnimationFactory.Add("Down", new SpriteSheetAnimationData(new[] { 4, 6, 4, 7 }));
            playerTwoAnimationFactory.Add("Left", new SpriteSheetAnimationData(new[] { 8, 10, 8, 11 }));
            playerTwoAnimationFactory.Add("Right", new SpriteSheetAnimationData(new[] { 12, 14, 12, 15 }));
            playerTwoAnimationFactory.Add("Idle", new SpriteSheetAnimationData(new[] { 4 }));
            gameEntities.Add(new Player(new Vector2(Config.GameScreenWidth / 2.0f, Config.GameScreenHeight / 2.0f),
                                        PreparePlayerSprites(playerTwoAnimationFactory),
                                        KeySchemaTemplatesFactory.getDefaultKeyboardPlayerTwoSchema(),
                                        items));

            var itemsSpritesheetTexture = this.Content.Load<Texture2D>("spritesheets/item_spritesheet");
            var itemsSpritesheetMap = Content.Load<Dictionary<string, Rectangle>>("spritesheets/itemdict");
            var itemsAtlas = new TextureAtlas("Items", itemsSpritesheetTexture, itemsSpritesheetMap);
            var itemsAnimationFactory = new SpriteSheetAnimationFactory(itemsAtlas);

            itemsAnimationFactory.Add("PiekarnikOpened", new SpriteSheetAnimationData(new[] { 1 }));
            itemsAnimationFactory.Add("PiekarnikClosed", new SpriteSheetAnimationData(new[] { 2 }));
            var piekarnikClosed = new AnimatedSprite(itemsAnimationFactory, "PiekarnikClosed");
            var piekarnikOpened = new AnimatedSprite(itemsAnimationFactory, "PiekarnikOpened");
            piekarnikClosed.Origin = piekarnikOpened.Origin = Vector2.Zero;
            piekarnikClosed.Position = new Vector2(145, 76);
            piekarnikOpened.Position = new Vector2(151, 76);
            items.Add("piekarnik", new Item(piekarnikClosed, piekarnikOpened));
     
            itemsAnimationFactory.Add("Swinka", new SpriteSheetAnimationData(new[] { 3, 4 }));
            var pigSpriteAnimation = new AnimatedSprite(itemsAnimationFactory, "Swinka");
            pigSpriteAnimation.Position = new Vector2(129, 61);
            pigSpriteAnimation.IsVisible = false;

            itemsAnimationFactory.Add("Kontakt", new SpriteSheetAnimationData(new[] { 0 }));
            kontaktSprite = new AnimatedSprite(itemsAnimationFactory, "Kontakt");
            kontaktSprite.Origin = Vector2.Zero;
            kontaktSprite.Position = new Vector2(136, 67);
        }


        private Dictionary<string, AnimatedSprite> PreparePlayerSprites(SpriteSheetAnimationFactory animationFactory)
        {
            var sprites = new Dictionary<string, AnimatedSprite>();
            sprites.Add("Idle", new AnimatedSprite(animationFactory, "Idle"));
            sprites.Add("Up", new AnimatedSprite(animationFactory, "Up"));
            sprites.Add("Down", new AnimatedSprite(animationFactory, "Down"));
            sprites.Add("Left", new AnimatedSprite(animationFactory, "Left"));
            sprites.Add("Right", new AnimatedSprite(animationFactory, "Right"));

            foreach (var sprite in sprites.Values) sprite.Origin = Vector2.Zero;

            return sprites;
        }


        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            keyboardState = Keyboard.GetState();

            foreach (var gameEntity in gameEntities) gameEntity.Update(gameTime);
            foreach (var item in items) item.Value.Update(gameTime);

            if (keyboardState.IsKeyDown(Keys.F10) && previousKeyboardState.IsKeyUp(Keys.F10))
            {
                graphics.ToggleFullScreen();
            }

            if (keyboardState.IsKeyDown(Keys.F9) && previousKeyboardState.IsKeyUp(Keys.F9))
            {
                debugFlag = !debugFlag;
            }

            previousKeyboardState = keyboardState;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            GraphicsDevice.SetRenderTarget(target);
            this.targetBatch.Begin();
            this.targetBatch.Draw(backgroundTexture, Vector2.Zero, Color.LawnGreen);
            foreach (var item in items) item.Value.Draw(this.targetBatch);
            this.targetBatch.Draw(kontaktSprite);
            // every drawn object should be sorted using isometric casting
            foreach (var gameEntity in gameEntities) gameEntity.Draw(this.targetBatch);
            if (debugFlag) DrawDebug();
            this.targetBatch.End();

            GraphicsDevice.SetRenderTarget(null);
            this.targetBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone);
            targetBatch.Draw(target, new Rectangle(0, 0, Config.ScaledGameScreenWidth, Config.ScaledGameScreenHeight), Color.White);
            this.targetBatch.End();

            base.Draw(gameTime);
        }

        protected void DrawDebug()
        {
            Map.Instance.DrawColliders(this.targetBatch, pixelTexture);
        }
    }
}
