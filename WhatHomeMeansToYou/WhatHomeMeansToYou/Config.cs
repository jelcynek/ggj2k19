﻿namespace WhatHomeMeansToYou
{
    // in future config should be loaded from file into that class
    static class Config
    {
        public const int ScaledGameScreenWidth = 1600;
        public const int ScaledGameScreenHeight = 900;
        public const int GameScreenWidth = 256;
        public const int GameScreenHeight = 144;
    }
}
