﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace WhatHomeMeansToYou
{
    public static class Util
    {
        public static bool IsInConvexPolygon(Vector2 testPoint, List<Vector2> polygon)
        {
            if (polygon.Count < 3) return false;
            //n>2 Keep track of cross product sign changes
            var pos = 0;
            var neg = 0;

            for (var i = 0; i < polygon.Count; i++)
            {
                //If point is in the polygon
                if (polygon[i] == testPoint)
                    return true;

                //Form a segment between the i'th point
                var x1 = polygon[i].X;
                var y1 = polygon[i].Y;

                //And the i+1'th, or if i is the last, with the first point
                var i2 = i < polygon.Count - 1 ? i + 1 : 0;

                var x2 = polygon[i2].X;
                var y2 = polygon[i2].Y;

                var x = testPoint.X;
                var y = testPoint.Y;

                //Compute the cross product
                var d = (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1);

                if (d > 0) pos++;
                if (d < 0) neg++;

                //If the sign changes, then point is outside
                if (pos > 0 && neg > 0)
                    return false;
            }

            //If no change in direction, then on same side of all segments, and thus inside
            return true;
        }
        public static void DrawPolygon(List<Vector2> polygonPoints, SpriteBatch spriteBatch, Texture2D texture)
        {
            Util.DrawLine(spriteBatch, polygonPoints[polygonPoints.Count - 1], polygonPoints[0], texture);
            for (int i = 1; i < polygonPoints.Count; i++)
            {
                Util.DrawLine(spriteBatch, polygonPoints[i - 1], polygonPoints[i], texture);
            }
        }

        public static void DrawLine(this SpriteBatch spriteBatch, Vector2 start, Vector2 end, Texture2D texture)
        {
            spriteBatch.Draw(texture, start, null, Color.White,
                             (float)Math.Atan2(end.Y - start.Y, end.X - start.X),
                             new Vector2(0f, (float)texture.Height / 2),
                             new Vector2(Vector2.Distance(start, end), 1f),
                             SpriteEffects.None, 0f);
        }
    }
}
