﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using WhatHomeMeansToYou.Interfaces;
using MonoGame.Extended.Animations;
using MonoGame.Extended.Animations.SpriteSheets;
using MonoGame.Extended.TextureAtlases;
using MonoGame.Extended.Sprites;

namespace WhatHomeMeansToYou
{
    class Player : IGameEntity
    {
        const float VELOCITY = 70.0F;
        const int COLLISION_FEET_SIZE = 5;
        readonly Dictionary<string, AnimatedSprite> sprites;
        KeySchema keySchema;
        AnimatedSprite sprite;
        Dictionary<string, Item> items;
        Vector2 position;

        public Player(Vector2 position, Dictionary<string, AnimatedSprite> sprites, KeySchema keySchema, Dictionary<string, Item> items)
        {
            this.keySchema = keySchema;
            this.sprites = sprites;
            this.sprite = sprites["Idle"];
            this.position = this.sprite.Position = position;
            // NAAAAAAAAASTY HACK, ITEMS LIST IS PASSED TO PLAYER BECAUSE NEEDS TO DO AN ACTION
            // IT COULD BE FIXED IN MANY WAY. SOME SINGLETON CONTAINER OR EVENT SYSTEM FOR COMMUNICATION
            // PROBABLY IN LONG RUN EVENT/MESSAGING SYSTEM WOULD BE BEST
            this.items = items;
        }

        // TODO: Moving part should be rewritten, there is something fishy here. Why we need to divide up and left directions and not down and right?
        // As well some code needs to be extracted
        public void Update(GameTime gameTime)
        {
            var keyboardState = Keyboard.GetState();
            var pressedKeys = keyboardState.GetPressedKeys().ToList();

            float velocity = VELOCITY * (float)gameTime.ElapsedGameTime.TotalSeconds;
            
            Vector2 newPosition = new Vector2(position.X, position.Y);
            var nothingPressed = false;
            if (keyboardState.IsKeyDown(keySchema.Left))
            {
                newPosition.X = (int)(position.X - velocity / 2.0f);
                if (sprite != sprites["Left"]) sprite = sprites["Left"];
            }
            else if (keyboardState.IsKeyDown(keySchema.Right))
            {
                newPosition.X = (int)(position.X + velocity);
                if (sprite != sprites["Right"]) sprite = sprites["Right"];
            }
            else
            {
                nothingPressed = true;
            }
            if (isPlayerNotColliding()) position = newPosition;

            newPosition = new Vector2(position.X, position.Y);
            if (keyboardState.IsKeyDown(keySchema.Up))
            {
                newPosition.Y = (int)(position.Y - velocity / 2.0f);
                if (sprite != sprites["Up"]) sprite = sprites["Up"];
            }
            else if (keyboardState.IsKeyDown(keySchema.Down))
            {
                newPosition.Y = (int)(position.Y + velocity);
                if (sprite != sprites["Down"]) sprite = sprites["Down"];
            }
            else
            {
                if (nothingPressed)
                {
                    if (sprite != sprites["Idle"]) sprite = sprites["Idle"];
                }
            }
            if (isPlayerNotColliding()) position = newPosition;

            if (pressedKeys.Exists(k => k == keySchema.Action))
            {
                DoAction(sprite);
            }

            sprite.Position = position;
            sprite.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite);
        }

        protected bool isPlayerNotColliding()
        {
            Vector2 leftFeet = new Vector2(position.X + sprite.BoundingRectangle.Width / 2 - COLLISION_FEET_SIZE, position.Y + sprite.BoundingRectangle.Height);
            Vector2 rightFeet = new Vector2(position.X + sprite.BoundingRectangle.Width / 2 + COLLISION_FEET_SIZE, position.Y + sprite.BoundingRectangle.Height);

            bool isInPlayArea = true;
            isInPlayArea = (Util.IsInConvexPolygon(leftFeet, Map.Instance.floorArea) &&
                            Util.IsInConvexPolygon(rightFeet, Map.Instance.floorArea));
            foreach (var collider in Map.Instance.listOfColliders)
            {
                if (!isInPlayArea) break;
                var collide = (Util.IsInConvexPolygon(leftFeet, collider) ||
                               Util.IsInConvexPolygon(rightFeet, collider));
                isInPlayArea = !collide;
            }
            return isInPlayArea;
        }

        protected void DoAction(AnimatedSprite player)
        {
            // action player one
            foreach (var item in items.Values)
            {
                if (player.BoundingRectangle.Intersects(item.sprite.BoundingRectangle))
                {
                    item.Action();
                    break;
                }
            }
        }

    }
}
